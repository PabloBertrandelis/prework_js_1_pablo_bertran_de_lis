function miFuncionDeCompra (num1, num2){
    console.log('Sus números son: '+ num1 + ', y ' + num2);
    if (num1 === 5){
        console.log('El primer número (' + num1 +') es estrictamente igual a 5.');
    }
    if (num1 <= num2){
        console.log('El primer número (' + num1 +') NO es mayor que el segundo número (' + num2 + ').');
    }
    if (num2 > 0){
        console.log('El segundo número (' + num2 + ') es positivo.');
    }
    if (num1 < 0 || num1 != 0){
        console.log('El primer número (' + num1 + ') es negativo o distinto de cero.');
    }
}
miFuncionDeCompra (5, 8);